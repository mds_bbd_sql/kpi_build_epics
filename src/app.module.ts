import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EpicsModule } from './epics/epics.module';

@Module({
  imports: [EpicsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
