import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';

@Controller('epics')
export class EpicsController {
  constructor() {}

  @Post()
  create(@Body() createEpicDto: CreateEpicDto) {
    return "TODO";
    /*
    return this.epicsService.create(createEpicDto);
    */
  }

  @Get()
  findAll() {
    return "TODO";
    /*
    return this.epicsService.findAll();
    */
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return "TODO";
    /*
    return this.epicsService.findOne(+id);
    */
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEpicDto: UpdateEpicDto) {
    return "TODO";
    /*
    return this.epicsService.update(+id, updateEpicDto);
    */
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return "TODO";
    /*
    return this.epicsService.remove(+id);
    */
  }
}
