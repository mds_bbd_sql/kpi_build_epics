import { Module } from '@nestjs/common';
import { EpicsController } from './epics.controller';

@Module({
  controllers: [EpicsController],
  providers: []
})
export class EpicsModule {}
